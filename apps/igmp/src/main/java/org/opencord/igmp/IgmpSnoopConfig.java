/*
 * Copyright 2015-present Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.opencord.igmp;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Maps;
import org.onlab.packet.MacAddress;
import org.onosproject.core.ApplicationId;
import org.onosproject.net.config.Config;
import org.onosproject.net.config.basics.BasicElementConfig;

import java.util.Map;

/**
 * Network config for the IGMP app.
 */
public class IgmpSnoopConfig extends Config<ApplicationId> {

    public class Info {
        private final MacAddress sourceMac;

        public Info(MacAddress sourceMac) {
            this.sourceMac = sourceMac;
        }

        public MacAddress sourceMac() {
            return sourceMac;
        }
    }

    private static final String CONNECTION_POINT = "connectionPoint";
    private static final String BINDINGS = "bindings";

    // Default connection point
    protected static final String DEFAULT_CONNECTION_POINT = "of:00000cc47ac2f5a3/4";

    /**
     * Gets the value of a string property, protecting for an empty
     * JSON object.
     *
     * @param name         name of the property
     * @param defaultValue default value if none has been specified
     * @return String value if one os found, default value otherwise
     */
    private String getStringProperty(String name, String defaultValue) {
        if (object == null) {
            return defaultValue;
        }

        return get(name, defaultValue);
    }

    /**
     * Returns the connection point on which to forward IGMP packets.
     *
     * @return provided value or default if not set
     */
    public String connectionPoint() {
        return getStringProperty(CONNECTION_POINT, DEFAULT_CONNECTION_POINT);
    }

    /**
     * Sets the connection point on which to forward IGMP packets.
     *
     * @param ofid new ofid to use as a connection point
     * @return self
     */
    public BasicElementConfig conectionPoint(String ofid) {
        return (BasicElementConfig) setOrClear(CONNECTION_POINT, ofid);
    }

    /**
     * Get source MAC bindings.
     *
     * @return the bindings from mac to forwarding mac
     */
    public Map<MacAddress, Info> info() {
        Map<MacAddress, Info> info = Maps.newHashMap();
        if (object == null) {
            return info;
        }
        JsonNode bindings = object.path(BINDINGS);
        bindings.forEach(jsonNode -> info.put(MacAddress.valueOf(jsonNode.path("mac").asText()),
            new Info(MacAddress.valueOf((String) jsonNode.path("sourceMac").asText()))));
        return info;
    }
}
