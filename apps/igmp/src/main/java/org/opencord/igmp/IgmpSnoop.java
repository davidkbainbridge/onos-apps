/*
 * Copyright 2015-present Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.opencord.igmp;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.onlab.packet.DeserializationException;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IGMP;
import org.onlab.packet.IGMPMembership;
import org.onlab.packet.MacAddress;
import org.onlab.packet.IGMPQuery;
import org.onlab.packet.IPv4;
import org.onlab.packet.Ip4Address;
import org.onlab.packet.IpAddress;
import org.onlab.packet.IpPrefix;
import org.onlab.util.SafeRecurringTask;
import org.onlab.util.Tools;
import org.onosproject.cfg.ComponentConfigService;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.DeviceId;
import org.onosproject.net.config.ConfigFactory;
import org.onosproject.net.config.NetworkConfigEvent;
import org.onosproject.net.config.NetworkConfigListener;
import org.onosproject.net.config.NetworkConfigRegistry;
import org.onosproject.net.config.basics.SubjectFactories;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.flowobjective.FlowObjectiveService;
import org.onosproject.net.mcast.McastRoute;
import org.onosproject.net.mcast.MulticastRouteService;
import org.onosproject.net.packet.DefaultOutboundPacket;
import org.onosproject.net.packet.InboundPacket;
import org.onosproject.net.packet.OutboundPacket;
import org.onosproject.net.packet.PacketContext;
import org.onosproject.net.packet.PacketProcessor;
import org.onosproject.net.packet.PacketService;
import org.opencord.cordconfig.access.AccessDeviceConfig;
import org.opencord.cordconfig.access.AccessDeviceData;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static org.onlab.util.Tools.groupedThreads;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Internet Group Management Protocol.
 */
@Component(immediate = true)
public class IgmpSnoop {

    private final Logger log = getLogger(getClass());

    private static final String DEST_MAC = "01:00:5E:00:00:01";
    private static final String DEST_IP = "224.0.0.1";

    private static final String DEFAULT_QUERY_GADDR = "0.0.0.0";
    private static final String DEFAULT_QUERY_SOURCE_IP = "1.1.1.1";
    private static final String DEFAULT_QUERY_SOURCE_MAC = "de:ad:be:ef:ba:11";
    private static final int DEFAULT_QUERY_PERIOD_SECS = 60;
    private static final byte DEFAULT_IGMP_RESP_CODE = 100;
    private static final byte DEFAULT_QUERY_QQIC = 125;
    private static final int DEFAULT_FORWARD_VLAN = 4000;
    private static final byte DEFAULT_FORWARD_PRIORITY = 5;
    private static final String DEFAULT_FORWARDING_SOURCE_MAC = "00:23:8A:F4:2A:00";

    @Property(name = "multicastAddress",
            label = "Define the multicast base range to listen to")
    private String multicastAddress = IpPrefix.IPV4_MULTICAST_PREFIX.toString();

    @Property(name = "queryPeriod", intValue = DEFAULT_QUERY_PERIOD_SECS,
            label = "Delay in seconds between successive query runs")
    private int queryPeriod = DEFAULT_QUERY_PERIOD_SECS;

    @Property(name = "maxRespCode", byteValue = DEFAULT_IGMP_RESP_CODE,
            label = "Maximum time allowed before sending a responding report")
    private byte maxRespCode = DEFAULT_IGMP_RESP_CODE;

    @Property(name = "queryQqic", byteValue = DEFAULT_QUERY_QQIC,
            label = "QQIC value for query packet")
    private byte queryQqic = DEFAULT_QUERY_QQIC;

    @Property(name = "querySourceIp", value = DEFAULT_QUERY_SOURCE_IP,
            label = "Source IP for query packet")
    private String querySourceIp = DEFAULT_QUERY_SOURCE_IP;

    @Property(name = "querySourceMac", value = DEFAULT_QUERY_SOURCE_MAC,
            label = "Source MAC for query packet")
    private String querySourceMac = DEFAULT_QUERY_SOURCE_MAC;

    @Property(name = "queryGaddr", value = DEFAULT_QUERY_GADDR,
            label = "gaddr for query packet")
    private String queryGaddr = DEFAULT_QUERY_GADDR;

    @Property(name = "forwardVlan", intValue = DEFAULT_FORWARD_VLAN,
            label = "VLAN on which to forward IGMP reports")
    private int forwardVlan = DEFAULT_FORWARD_VLAN;

    @Property(name = "forwardPriority", byteValue = DEFAULT_FORWARD_PRIORITY,
            label = "Priority at which to forward IGMP reports")
    private byte forwardPriority = DEFAULT_FORWARD_PRIORITY;

    @Property(name = "defaultForwardingSourceMac", value = DEFAULT_FORWARDING_SOURCE_MAC,
            label = "MAC used as source when forwarding IGMP packets if not mapping found")
    private String defaultForwardingSourceMac = DEFAULT_FORWARDING_SOURCE_MAC;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected FlowObjectiveService flowObjectiveService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected PacketService packetService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected CoreService coreService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected NetworkConfigRegistry networkConfig;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected ComponentConfigService componentConfigService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected MulticastRouteService multicastService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected DeviceService deviceService;

    private ScheduledFuture<?> queryTask;
    private final ScheduledExecutorService queryService =
            Executors.newSingleThreadScheduledExecutor(groupedThreads("onos/igmp-query",
                                                                      "membership-query"));

    private Map<DeviceId, AccessDeviceData> oltData = new ConcurrentHashMap<>();

    private Map<IpAddress, IpAddress> ssmTranslateTable = new ConcurrentHashMap<>();

    private String connectionPoint = null;
    private Map<MacAddress, IgmpSnoopConfig.Info> snoopInfo = null;

    private IgmpPacketProcessor processor = new IgmpPacketProcessor();
    private static ApplicationId appId;

    private InternalNetworkConfigListener configListener =
            new InternalNetworkConfigListener();

    private static final Class<AccessDeviceConfig> CONFIG_CLASS =
            AccessDeviceConfig.class;

    private static final Class<IgmpSnoopConfig> IGMP_SNOOP_CONFIG_CLASS =
            IgmpSnoopConfig.class;

    private static final Class<IgmpSsmTranslateConfig> SSM_TRANSLATE_CONFIG_CLASS =
            IgmpSsmTranslateConfig.class;

    private final ConfigFactory<DeviceId, AccessDeviceConfig> configFactory =
            new ConfigFactory<DeviceId, AccessDeviceConfig>(
                    SubjectFactories.DEVICE_SUBJECT_FACTORY, CONFIG_CLASS, "accessDevice") {
                @Override
                public AccessDeviceConfig createConfig() {
                    return new AccessDeviceConfig();
                }
            };


    private final ConfigFactory<ApplicationId, IgmpSnoopConfig> igmpSnoopConfigFactory =
            new ConfigFactory<ApplicationId, IgmpSnoopConfig>(SubjectFactories.APP_SUBJECT_FACTORY,
                                                              IgmpSnoopConfig.class, "snoop") {
                @Override
                public IgmpSnoopConfig createConfig() {
                    log.info("CREATING snoop configuration");
                    return new IgmpSnoopConfig();
                }
            };

    private ConfigFactory<ApplicationId, IgmpSsmTranslateConfig> ssmTranslateConfigFactory =
            new ConfigFactory<ApplicationId, IgmpSsmTranslateConfig>(
                    SubjectFactories.APP_SUBJECT_FACTORY, SSM_TRANSLATE_CONFIG_CLASS, "ssmTranslate", true) {
                @Override
                public IgmpSsmTranslateConfig createConfig() {
                    return new IgmpSsmTranslateConfig();
                }
            };


    private ByteBuffer queryPacket;


    @Activate
    public void activate(ComponentContext context) {
        componentConfigService.registerProperties(getClass());
        modified(context);

        networkConfig.registerConfigFactory(configFactory);
        networkConfig.registerConfigFactory(ssmTranslateConfigFactory);
        networkConfig.registerConfigFactory(igmpSnoopConfigFactory);

        appId = coreService.registerApplication("org.opencord.igmp");

        networkConfig.getConfig(appId, IgmpSnoopConfig.class);

        networkConfig.addListener(configListener);

        networkConfig.getSubjects(DeviceId.class, AccessDeviceConfig.class).forEach(
                subject -> {
                    AccessDeviceConfig config = networkConfig.getConfig(subject,
                                                                        AccessDeviceConfig.class);
                    if (config != null) {
                        AccessDeviceData data = config.getOlt();
                        oltData.put(data.deviceId(), data);

                    }
                }
        );

        IgmpSsmTranslateConfig ssmTranslateConfig =
                networkConfig.getConfig(appId, IgmpSsmTranslateConfig.class);

        if (ssmTranslateConfig != null) {
            Collection<McastRoute> translations = ssmTranslateConfig.getSsmTranslations();
            for (McastRoute route : translations) {
                ssmTranslateTable.put(route.group(), route.source());
            }
        }

        IgmpSnoopConfig igmpSnoopConfig =
            networkConfig.getConfig(appId, IgmpSnoopConfig.class);

        restartQueryTask();

        packetService.addProcessor(processor, PacketProcessor.director(1));

        log.info("Started");
    }

    @Deactivate
    public void deactivate() {
        packetService.removeProcessor(processor);
        processor = null;
        networkConfig.removeListener(configListener);
        networkConfig.unregisterConfigFactory(configFactory);
        networkConfig.unregisterConfigFactory(ssmTranslateConfigFactory);
        networkConfig.unregisterConfigFactory(igmpSnoopConfigFactory);
        queryTask.cancel(true);
        queryService.shutdownNow();
        componentConfigService.unregisterProperties(getClass(), false);
        log.info("Stopped");
    }

    @Modified
    protected void modified(ComponentContext context) {
        Dictionary<?, ?> properties = context != null ? context.getProperties() : new Properties();

        // TODO read multicastAddress from config
        String strQueryPeriod = Tools.get(properties, "queryPeriod");
        String strResponseCode = Tools.get(properties, "maxRespCode");
        String strQueryQqic = Tools.get(properties, "queryQqic");
        String newQuerySourceIp = Tools.get(properties, "querySourceIp");
        String newQuerySourceMac = Tools.get(properties, "querySourceMac");
        String newQueryGaddr = Tools.get(properties, "queryGaddr");
        String strForwardVlan = Tools.get(properties, "forwardVlan");
        String strForwardPriority = Tools.get(properties, "forwardPriority");
        String newDefaultForwardingSourceMac = Tools.get(properties, "defaultForwardingSourceMac");
        try {
            boolean rebuildPacket = false;

            byte newMaxRespCode = Byte.parseByte(strResponseCode);
            if (maxRespCode != newMaxRespCode) {
                maxRespCode = newMaxRespCode;
                rebuildPacket = true;
            }

            byte newQueryQqic = Byte.parseByte(strQueryQqic);
            if (queryQqic != newQueryQqic) {
                queryQqic = newQueryQqic;
                rebuildPacket = true;
            }

            int newForwardVlan = Integer.parseInt(strForwardVlan);
            if (forwardVlan != newForwardVlan) {
                forwardVlan = newForwardVlan;
                rebuildPacket = true;
            }

            byte newForwardPriority = Byte.parseByte(strForwardPriority);
            if (forwardPriority != newForwardPriority) {
                forwardPriority = newForwardPriority;
                rebuildPacket = true;
            }

            if (!defaultForwardingSourceMac.equals(newDefaultForwardingSourceMac)) {
                defaultForwardingSourceMac = newDefaultForwardingSourceMac;
            }

            if (!querySourceIp.equals(newQuerySourceIp)) {
                querySourceIp = newQuerySourceIp;
                rebuildPacket = true;
            }

            if (!querySourceMac.equals(newQuerySourceMac)) {
                querySourceMac = newQuerySourceMac;
                rebuildPacket = true;
            }

            if (!queryGaddr.equals(newQueryGaddr)) {
                queryGaddr = newQueryGaddr;
                rebuildPacket = true;
            }

            if (rebuildPacket) {
                queryPacket = buildQueryPacket();
            }

            int newQueryPeriod = Integer.parseInt(strQueryPeriod);
            if (newQueryPeriod != queryPeriod) {
                queryPeriod = newQueryPeriod;
                restartQueryTask();
            }

        } catch (NumberFormatException e) {
            log.warn("Error parsing config input", e);
        }

        log.info("queryPeriod set to {}", queryPeriod);
        log.info("maxRespCode set to {}", maxRespCode);
        log.info("queryQqic set to {}", queryQqic);
        log.info("querySourceMac set to {}", querySourceMac);
        log.info("querySourceIp set to {}", querySourceIp);
        log.info("queryGaddr set to {}", queryGaddr);
    }

    private void restartQueryTask() {
        if (queryTask != null) {
            queryTask.cancel(true);
        }
        queryPacket = buildQueryPacket();
        queryTask = queryService.scheduleWithFixedDelay(
                SafeRecurringTask.wrap(this::querySubscribers),
                0,
                queryPeriod,
                TimeUnit.SECONDS);
    }


    private void processMembership(IGMP pkt, ConnectPoint location) {
        pkt.getGroups().forEach(group -> {

            if (!(group instanceof IGMPMembership)) {
                log.warn("Wrong group type in IGMP membership");
                return;
            }

            IGMPMembership membership = (IGMPMembership) group;

            IpAddress groupAddress = membership.getGaddr();

            if (membership.getRecordType() == IGMPMembership.MODE_IS_INCLUDE ||
                    membership.getRecordType() == IGMPMembership.CHANGE_TO_INCLUDE_MODE) {

                if (membership.getSources().isEmpty()) {
                    McastRoute route = ssmTranslateRoute(groupAddress);
                    if (route != null) {
                        removeRoute(route, location);
                    }
                } else {
                    membership.getSources().stream()
                            .map(source -> new McastRoute(source, groupAddress, McastRoute.Type.IGMP))
                            .forEach(route -> addRoute(route, location));
                }
            } else if (membership.getRecordType() == IGMPMembership.MODE_IS_EXCLUDE ||
                    membership.getRecordType() == IGMPMembership.CHANGE_TO_EXCLUDE_MODE) {

                if (membership.getSources().isEmpty()) {
                    McastRoute route = ssmTranslateRoute(groupAddress);
                    if (route != null) {
                        addRoute(route, location);
                    }
                } else {
                    membership.getSources().stream()
                            .map(source -> new McastRoute(source, groupAddress, McastRoute.Type.IGMP))
                            .forEach(route -> removeRoute(route, location));
                }
            }
        });
    }

    private McastRoute ssmTranslateRoute(IpAddress group) {
        IpAddress source = IpAddress.valueOf("1.1.1.1"); //Mocking this because this PoC does not do PIM.
        if (source == null) {
            log.warn("No SSM translate source found for group {}", group);
            return null;
        }
        return new McastRoute(source, group, McastRoute.Type.IGMP);
    }

    private void addRoute(McastRoute route, ConnectPoint location) {
        multicastService.add(route);
        multicastService.addSink(route, location);
    }

    private void removeRoute(McastRoute route, ConnectPoint location) {
        multicastService.removeSink(route, location);
        // TODO remove route if all sinks are gone
    }

    private ByteBuffer buildQueryPacket() {
        IGMP igmp = new IGMP.IGMPv3();
        igmp.setIgmpType(IGMP.TYPE_IGMPV3_MEMBERSHIP_QUERY);
        igmp.setMaxRespCode(maxRespCode);

        IGMPQuery query = new IGMPQuery(IpAddress.valueOf(queryGaddr), 0);
        query.setQqic((byte) queryQqic);
        igmp.addGroup(query);

        IPv4 ip = new IPv4();
        ip.setDestinationAddress(DEST_IP);
        ip.setProtocol(IPv4.PROTOCOL_IGMP);
        //ip.setSourceAddress("192.168.1.1");
        ip.setSourceAddress(querySourceIp);
        ip.setTtl((byte) 1);
        ip.setPayload(igmp);
        // Add a router alert option
        ip.setOptions(new byte[]{(byte) 0x94, (byte) 0x04, (byte) 0x00, (byte) 0x00});

        Ethernet eth = new Ethernet();
        eth.setDestinationMACAddress(DEST_MAC);
        eth.setSourceMACAddress(querySourceMac);
        eth.setEtherType(Ethernet.TYPE_IPV4);

        eth.setPayload(ip);
        eth.setPad(true);

        return ByteBuffer.wrap(eth.serialize());
    }

    private void querySubscribers() {
        oltData.keySet().stream()
                .flatMap(did -> deviceService.getPorts(did).stream())
                .filter(p -> !oltData.get(p.element().id()).uplink().equals(p.number()))
                .filter(p -> p.isEnabled())
                .forEach(p -> {
                    TrafficTreatment treatment = DefaultTrafficTreatment.builder()
                            .setOutput(p.number()).build();
                    packetService.emit(new DefaultOutboundPacket((DeviceId) p.element().id(),
                                                                 treatment, queryPacket));
                });
    }

    /**
     * Packet processor responsible for handling IGMP packets.
     */
    private class IgmpPacketProcessor implements PacketProcessor {

        @Override
        public void process(PacketContext context) {
            // Stop processing if the packet has been handled, since we
            // can't do any more to it.
            if (context.isHandled()) {
                return;
            }

            InboundPacket pkt = context.inPacket();
            Ethernet ethPkt = pkt.parsed();
            if (ethPkt == null) {
                return;
            }


            // IPv6 MLD packets are handled by ICMP6. We'll only deal with IPv4.
            if (ethPkt.getEtherType() != Ethernet.TYPE_IPV4) {
                return;
            }

            IPv4 ip = (IPv4) ethPkt.getPayload();
            IpAddress gaddr = IpAddress.valueOf(ip.getDestinationAddress());
            IpAddress saddr = Ip4Address.valueOf(ip.getSourceAddress());
            log.trace("Packet ({}, {}) -> ingress port: {}", saddr, gaddr,
                      context.inPacket().receivedFrom());


            if (ip.getProtocol() != IPv4.PROTOCOL_IGMP ||
                    !IpPrefix.IPV4_MULTICAST_PREFIX.contains(gaddr)) {
                return;
            }

            if (IpPrefix.IPV4_MULTICAST_PREFIX.contains(saddr)) {
                log.debug("IGMP Picked up a packet with a multicast source address.");
                return;
            }


            //FIXME: The connect point should not be hardcoded

            ByteBuffer byteBuffer = context.inPacket().unparsed();
            DhcpEthernet packet = null;
            try {
                packet = DhcpEthernet.deserializer().deserialize(byteBuffer.array(), 0, byteBuffer.array().length);
            } catch (DeserializationException e) {
                log.warn("Unable to deserialize packet");
            }
            // AAA ConnectPoint connectPoint = ConnectPoint.deviceConnectPoint("of:00000cc47ac2f5a3/4");
            log.info("forwarding to connection point {}", connectionPoint);
            ConnectPoint connectPoint = ConnectPoint.deviceConnectPoint(connectionPoint);
            TrafficTreatment t = DefaultTrafficTreatment.builder()
                    .setOutput(connectPoint.port()).build();

            // packet.setQinQVID((short) 2);
            // packet.setVlanID((short) 2);
            // OutboundPacket o = new DefaultOutboundPacket(
            //        connectPoint.deviceId(), t, ByteBuffer.wrap(packet.serialize()));

            // IGMP.IGMPv2 v2 = new IGMP.IGMPv2();
            // IGMP.IGMPv3 v3 = (IGMP.IGMPv3) ethPkt.getPayload().getPayload();
            // v3.getGroups().forEach(g -> v2.addGroup(g));
            // v2.setMaxRespCode(v3.getMaxRespField());
            // v2.setIgmpType(IGMP.TYPE_IGMPV2_MEMBERSHIP_REPORT);
            // v2.resetChecksum();
            // ethPkt.getPayload().setPayload(v2);

            IGMP.IGMPv3 v3 = (IGMP.IGMPv3) ethPkt.getPayload().getPayload();
            v3.resetChecksum();
            if (forwardPriority != -1) {
                ethPkt.setPriorityCode(forwardPriority);
            }

            IgmpSnoopConfig.Info info = snoopInfo.get(ethPkt.getSourceMAC());
            log.info("looking for source MAC forwarding map for {}, found {}",
                ethPkt.getSourceMAC().toString(), info);
            MacAddress sourceMac = null;
            if (info == null) {
                log.warn("no mapping for MAC address {} found, using default {}",
                    ethPkt.getSourceMAC().toString(), DEFAULT_FORWARDING_SOURCE_MAC);
                sourceMac = MacAddress.valueOf(DEFAULT_FORWARDING_SOURCE_MAC);
            } else {
                sourceMac = info.sourceMac();
            }
            ethPkt.setSourceMACAddress(sourceMac);
            ethPkt.getPayload().getPayload().resetChecksum();
            ethPkt.setPad(true);

            if (forwardVlan != -1) {
                ethPkt.setVlanID((short) forwardVlan);
            }
            OutboundPacket o = new DefaultOutboundPacket(
                    connectPoint.deviceId(), t, ByteBuffer.wrap(ethPkt.serialize()));
            if (log.isTraceEnabled()) {
                log.trace("Forwarding igmp packet", ethPkt, connectPoint);
            }
            packetService.emit(o);

            IGMP igmp = (IGMP) ip.getPayload();
            switch (igmp.getIgmpType()) {
                case IGMP.TYPE_IGMPV3_MEMBERSHIP_REPORT:
                    log.debug("DKB: processing membership report {} from {}",
                              igmp, pkt.receivedFrom());
                    processMembership(igmp, pkt.receivedFrom());
                    break;

                case IGMP.TYPE_IGMPV3_MEMBERSHIP_QUERY:
                    log.debug("Received a membership query {} from {}",
                              igmp, pkt.receivedFrom());
                    break;

                case IGMP.TYPE_IGMPV1_MEMBERSHIP_REPORT:
                case IGMP.TYPE_IGMPV2_MEMBERSHIP_REPORT:
                case IGMP.TYPE_IGMPV2_LEAVE_GROUP:
                    log.debug("IGMP version 1 & 2 message types are not currently supported. Message type: {}",
                              igmp.getIgmpType());
                    break;
                default:
                    log.warn("Unknown IGMP message type: {}", igmp.getIgmpType());
                    break;
            }
        }
    }


    private class InternalNetworkConfigListener implements NetworkConfigListener {
        @Override
        public void event(NetworkConfigEvent event) {
            log.info("configuration event: {}", event.type());
            switch (event.type()) {

                case CONFIG_ADDED:
                case CONFIG_UPDATED:
                    log.info("ADDED OR UPDATED");

                    if (event.configClass().equals(SSM_TRANSLATE_CONFIG_CLASS)) {
                        IgmpSsmTranslateConfig config =
                                networkConfig.getConfig((ApplicationId) event.subject(),
                                                        SSM_TRANSLATE_CONFIG_CLASS);

                        if (config != null) {
                            ssmTranslateTable.clear();
                            config.getSsmTranslations().forEach(
                                    route -> ssmTranslateTable.put(route.group(), route.source()));
                        }
                    } else if (event.configClass().equals(IgmpSnoopConfig.class)) {
                        log.info("updaing configuration for IGMP Snoop Configuration");
                        IgmpSnoopConfig config =
                                networkConfig.getConfig(appId, IgmpSnoopConfig.class);
                        log.info("GOT RESULT {}", config);
                        if (config != null) {
                            connectionPoint = config.connectionPoint();
                            snoopInfo = config.info();
                        }
                    }
                    break;
                case CONFIG_REGISTERED:
                case CONFIG_UNREGISTERED:
                    log.info("REG OR UNREG");
                    break;
                case CONFIG_REMOVED:
                    log.info("REMOVED");
                    if (event.configClass().equals(SSM_TRANSLATE_CONFIG_CLASS)) {
                        ssmTranslateTable.clear();
                    } else if (event.configClass().equals(CONFIG_CLASS)) {
                        oltData.remove(event.subject());
                    }

                default:
                    log.info("UNKNOWN");
                    break;
            }
        }
    }
}
